#!/bin/ash
if [ -n "$MASTER_URL" ]; then
    certificate=$(base64 /var/run/secrets/kubernetes.io/serviceaccount/ca.crt | sed ':a;N;$!ba;s/\n//g')
    usertoken=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
    echo "apiVersion: v1
kind: Config
preferences: {}
# Define the cluster
clusters:
- cluster:
    certificate-authority-data: $certificate
    server: https://$MASTER_URL
  name: kilo
# Define the user
users:
- name: kilo
  user:
    token: $usertoken
# Define the context: linking a user to a cluster
contexts:
- context:
    cluster: kilo
    namespace: kube-system
    user: kilo
  name: kilo
# Define current context
current-context: kilo" > /etc/kubernetes/kubeconfig
fi
